using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class MobileInputHandler : InputHandler
{
    [SerializeField] private MyButton _thrustButton;
    [SerializeField] private MyButton _leftButton;
    [SerializeField] private MyButton _rightButton;

    protected override void SetLeft() => _left = _leftButton.IsPressed;
    protected override void SetRight() => _right = _rightButton.IsPressed;
    protected override void SetThrust() => _thrust = _thrustButton.IsPressed;
}

