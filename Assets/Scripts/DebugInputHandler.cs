using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugInputHandler : InputHandler
{
    [SerializeField] private List<InputHandler> _listOfHandlers;

    private void Start()
    {
        _listOfHandlers.AddRange(GetComponents<InputHandler>());
        _listOfHandlers.Remove(this);
    }

    protected override void SetLeft()
    {
        _left = false;
        foreach (var item in _listOfHandlers)
            _left |= item.Left;
    }

    protected override void SetRight()
    {
        _right = false;
        foreach (var item in _listOfHandlers)
            _right |= item.Right;
    }


    protected override void SetThrust()
    {
        _thrust = false;
        foreach (var item in _listOfHandlers)
            _thrust |= item.Thrust;

    }


}
