using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class InputHandler : MonoBehaviour
{
    protected bool _left;
    protected bool _right;
    protected bool _thrust;

    public bool Left { get => _left; }
    public bool Right { get => _right; }
    public bool Thrust { get => _thrust; }

    protected abstract void SetLeft();
    protected abstract void SetRight();
    protected abstract void SetThrust();

    protected virtual void Update()
    {
        SetLeft();
        SetRight();
        SetThrust();
    }
}
