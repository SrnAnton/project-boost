using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour
{
    [SerializeField] private Vector3 _movementVector;
    [SerializeField] float period = 2.0f;   // in sec

    private Vector3 _startingPosition;
    private float _movementFactor;

    const float _tau = Mathf.PI * 2.0f;

    private void Start()
    {
        _startingPosition = transform.position;
    }

    private void Update()
    {
        if (period <= Mathf.Epsilon)
            return;

        Move();
    }

    private void Move()
    {
        float cycle = Time.time / period;
        _movementFactor = (Mathf.Sin(cycle * _tau) + 1.0f) / 2.0f;
        Vector3 offset = _movementVector * _movementFactor;
        transform.position = _startingPosition + offset;
    }
}

