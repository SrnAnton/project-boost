using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public sealed class KeyboardInputHandler : InputHandler
{
    // Example new Unity input system
    // https://docs.unity3d.com/Packages/com.unity.inputsystem@1.0/manual/QuickStartGuide.html
    [SerializeField] private InputAction _input;

    protected override void SetLeft() => _left = _input.ReadValue<Vector2>().x < -float.Epsilon;
    protected override void SetRight() => _right = _input.ReadValue<Vector2>().x > float.Epsilon;
    protected override void SetThrust() => _thrust = _input.ReadValue<Vector2>().y > float.Epsilon;
    private void OnEnable() => _input.Enable();
    private void OnDisable() => _input.Disable();

    // Old
    //// Input.GetAxis("Horizontal")
    //// Input.GetAxis("Vertical")
    // protected override void SetLeft() => _left = Input.GetKey(KeyCode.A);
    // protected override void SetRight() => _right = Input.GetKey(KeyCode.D);
    // protected override void SetThrust() => _thrust = Input.GetKey(KeyCode.Space);
}
