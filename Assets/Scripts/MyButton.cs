using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MyButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private bool _isPressed;
    public bool IsPressed
    {
        get => _isPressed;
        private set => _isPressed = value;
    }

    public void OnPointerDown(PointerEventData eventData) => IsPressed = true;
    public void OnPointerUp(PointerEventData eventData) => IsPressed = false;
}
