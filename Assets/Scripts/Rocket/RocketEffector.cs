using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public sealed class RocketEffector : RocketResponder
{
    [SerializeField] private ParticleSystem _crash;
    [SerializeField] private ParticleSystem _mainNozzle;
    [SerializeField] private ParticleSystem _leftNozzle;
    [SerializeField] private ParticleSystem _rightNozzle;
    [SerializeField] private ParticleSystem _success;

    protected override void OnThrusted() => _mainNozzle.Play();
    protected override void OnLeftTurned() => _leftNozzle.Play();
    protected override void OnRightTurned() => _rightNozzle.Play();
    protected override void OnSucceed() => _success.Play();
    protected override void OnCrashed() => _crash.Play();
    protected override void OnIdled() => _mainNozzle.Stop();
    protected override void OnFixed()
    {
        _leftNozzle.Stop();
        _rightNozzle.Stop();
    }
}
