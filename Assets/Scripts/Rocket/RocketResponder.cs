using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rocket))]
abstract public class RocketResponder : MonoBehaviour
{
  

    protected Rocket _rocket;

    protected virtual void Awake()
    {
        _rocket = GetComponent<Rocket>();
    }

    protected virtual void OnEnable()
    {
        _rocket.LeftTurned.AddListener(OnLeftTurned);
        _rocket.RightTurned.AddListener(OnRightTurned);
        _rocket.Fixed.AddListener(OnFixed);
        _rocket.Thrusted.AddListener(OnThrusted);
        _rocket.Idled.AddListener(OnIdled);
        _rocket.Crashed.AddListener(OnCrashed);
        _rocket.Succeed.AddListener(OnSucceed);
    }

    protected virtual void OnDisable()
    {
        _rocket.LeftTurned.RemoveListener(OnLeftTurned);
        _rocket.RightTurned.RemoveListener(OnRightTurned);
        _rocket.Fixed.RemoveListener(OnFixed);
        _rocket.Thrusted.RemoveListener(OnThrusted);
        _rocket.Idled.RemoveListener(OnIdled);
        _rocket.Crashed.RemoveListener(OnCrashed);
        _rocket.Succeed.RemoveListener(OnSucceed);
    }

    protected virtual void OnThrusted() { }
    protected virtual void OnLeftTurned() { }
    protected virtual void OnRightTurned() { }
    protected virtual void OnSucceed() { }
    protected virtual void OnCrashed() { }
    protected virtual void OnIdled() { }
    protected virtual void OnFixed() { }
}
