using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(RocketMover))]
public class RocketFuelSystem : RocketResponder
{
    public UnityEvent Ended;

    [SerializeField] private float _capacity;
    [SerializeField] private float _consumption;

    private float TurnCapacity => _consumption / 10.0f;
    private float _value;
    private bool _isThrusted = false;
    private bool _isFixeding = false;
    private bool _isEnded = false;

    private void Start()
    {
        _value = _capacity;
    }

    private void FixedUpdate()
    {
        if (_isEnded)
            return;

        if (_value <= 0.0f)
        {
            _isEnded = true;
            Ended?.Invoke();
        }

        if (_isThrusted)
            _value -= _consumption * Time.fixedDeltaTime;

        if (!_isFixeding)
            _value -= TurnCapacity * Time.fixedDeltaTime;
    }

    protected override void OnThrusted() => _isThrusted = true;
    protected override void OnIdled() => _isThrusted = false;

    protected override void OnFixed() => _isFixeding = true;
    protected override void OnLeftTurned() => _isFixeding = false;
    protected override void OnRightTurned() => _isFixeding = false;

}
