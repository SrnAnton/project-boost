using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class RocketLight : RocketResponder
{
    [SerializeField] private Light _main;
    [SerializeField] private Light _left;
    [SerializeField] private Light _right;

    protected override void Awake()
    {
        base.Awake();

        _main.enabled = false;
        _left.enabled = false;
        _right.enabled = false;      
    }

    protected override void OnThrusted() => _main.enabled = true;
    protected override void OnLeftTurned() => _right.enabled = true;   
    protected override void OnRightTurned() => _left.enabled = true;
    protected override void OnIdled() => _main.enabled = false;
    protected override void OnFixed()
    {
        _left.enabled = false;
        _right.enabled = false;
    }
}
