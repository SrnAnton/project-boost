using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public sealed class RocketMover : RocketResponder
{
    private enum TurnState
    {
        None = 0,
        Left = 1,
        Right = -1
    }

    [SerializeField] private float _mainThrust = 1.0f;
    [SerializeField] private float _rotationThrust = 1.0f;

    private TurnState _state = TurnState.None;
    private Rigidbody _rigidbody;
    private bool _isThrusted = false;

    protected override void Awake()
    {
        base.Awake();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (_isThrusted)
            _rigidbody.AddRelativeForce(Vector3.up * _mainThrust * Time.deltaTime, ForceMode.Force);

        if (_state != TurnState.None)
            ApplyRotation(_rotationThrust * (float)_state);
    }

    protected override void OnThrusted() => _isThrusted = true;
    protected override void OnIdled() => _isThrusted = false;
    protected override void OnFixed() => _state = TurnState.None;
    protected override void OnLeftTurned() => _state = TurnState.Left;
    protected override void OnRightTurned() => _state = TurnState.Right;

    private void ApplyRotation(float rotationThisFrame)
    {
        _rigidbody.freezeRotation = true;
        transform.Rotate(Vector3.forward * rotationThisFrame * Time.deltaTime);
        _rigidbody.freezeRotation = false;
    }
}

