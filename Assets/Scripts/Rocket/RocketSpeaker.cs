
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class RocketSpeaker : RocketResponder
{
    [SerializeField] private AudioClip _engineWork;
    [SerializeField] private AudioClip _crash;
    [SerializeField] private AudioClip _success;

    private AudioSource _audioSource;
    
    protected override void Awake()
    {
        base.Awake();
        _audioSource = GetComponent<AudioSource>();   
    }

    protected override void OnThrusted() =>  _audioSource.PlayOneShot(_engineWork);
    protected override void OnIdled() => _audioSource.Stop();
    protected override void OnCrashed() => StartClip(_crash);
    protected override void OnSucceed() => StartClip(_success);
  
    private void StartClip(AudioClip clip)
    {
        _audioSource.Stop();
        _audioSource.PlayOneShot(clip);
    }


}
