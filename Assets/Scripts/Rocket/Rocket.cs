using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Rocket : MonoBehaviour
{
    [SerializeField] private InputHandler _input;

    public UnityEvent Thrusted;
    public UnityEvent Idled;
    public UnityEvent LeftTurned;
    public UnityEvent RightTurned;
    public UnityEvent Fixed;
    public UnityEvent Crashed;
    public UnityEvent Succeed;

    private bool _isTransitioning = false;
    private bool _isIdling;
    private bool _isFixeding;

    private void Update()
    {
        if (_isTransitioning)
            return;

        Thrust();
        Turn();
    }

    private void Thrust()
    {
        if (_input.Thrust && _isIdling)
        {
            Thrusted?.Invoke();
            _isIdling = false;
        }
        else if (!_input.Thrust && !_isIdling)
        {
            Idled?.Invoke();
            _isIdling = true;
        }
    }

    private void Turn()
    {
        if (!_input.Right && _input.Left && _isFixeding)
        {
            LeftTurned?.Invoke();
            _isFixeding = false;
        }
        else if (_input.Right && !_input.Left && _isFixeding)
        {
            RightTurned?.Invoke();
            _isFixeding = false;
        }
        else if ((!_input.Left && !_input.Right && !_isFixeding) || (_input.Right && _input.Left && !_isFixeding))
        {
            Fixed?.Invoke();
            _isFixeding = true;
        }
    }

    public void OnFuelEnd()
    {
        Idled?.Invoke();
        Fixed?.Invoke();
        _isTransitioning = true;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (_isTransitioning)
            return;

        if (other.gameObject.TryGetComponent(out Obstacle _))
        {
            _isTransitioning = true;
            Crashed?.Invoke();
        }
        else if (other.gameObject.TryGetComponent(out Finish _))
        {
            _isTransitioning = true;
            Succeed?.Invoke();
        }
    }
}