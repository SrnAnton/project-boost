using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IJunior.TypedScenes; 

public class LevelSwitcher : MonoBehaviour
{
    [SerializeField] private float _delayTime = 1.0f;

    public void LoadCurrentLevel()
    {
        Invoke("LoadLevel", _delayTime);
    }

    private void LoadLevel()
    {
        Sandbox.Load();
    }
}
