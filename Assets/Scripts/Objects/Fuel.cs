using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fuel : MonoBehaviour
{
    public void Break()
    {
        Destroy(gameObject);
    }
}
